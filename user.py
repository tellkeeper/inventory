import psycopg2

from psycopg2.extras import RealDictCursor
from werkzeug.security import generate_password_hash, check_password_hash
from creds import connect


class User(object):
    def __init__(self, row):
        self.username = row['folkname']
        self.password = row['pass_word']
        self.email = row['email']
    #def set_password(self, password):
     #   self.pw_hash = generate_password_hash(password)
    #def check_password(self, password):
    #    return check_password(self.pw_hash, password)

class UserDatabase:
    def __init__(self, conn):
        self.conn = conn

    def store(self, user, passw, email):
        query = """INSERT INTO folks (folkname, pass_word, email) VALUES (%s, %s, %s)"""
        curs = self.conn.cursor()
        curs.execute(query, (user, passw, email))

    def load(self, username):
        query = """SELECT *
                FROM folks
                WHERE (folkname = %(username)s OR %(username)s IS NULL)
                """
        curs = self.conn.cursor(cursor_factory=RealDictCursor)
        curs.execute(query,dict(username=username))
        row = curs.fetchone()
        if row:
            return User(row)
        return None

    #def logout():
        #destroy
