import psycopg2

from psycopg2.extras import RealDictCursor
from creds import connect

class Item:
    def __init__(self, row):
        self.ver = row['ver']
        self.manu = row['manu']
        self.model = row['model']

class ItemDatabase:
    def __init__(self, conn):
        self.conn = conn

    def push(self, item):
        query = """INSERT INTO pg_equipment (ver, manu, model) VALUES (%s, %s, %s)"""
        curs = self.conn.cursor(cursor_factory=RealDictCursor)
        curs.execute(query, (item.ver, item.manu, item.model))

    def pull(self, ver=None, manu=None, make=None, model=None):
        query = """SELECT * from pg_equipment"""
        curs = self.conn.cursor(cursor_factory=RealDictCursor)
        curs.execute(
            query,
            dict(
                ver=ver,
                manu=manu,
                model=model,
            ))
        rows = curs.fetchall()
        if rows:
            return [Item(row) for row in rows]