import psycopg2
import os
from flask import Flask, render_template, request
from item import Item, ItemDatabase
from user import User, UserDatabase, connect

app = Flask(__name__)

@app.route('/', methods=["GET", "POST"])
def home():    
    if request.form:
        new_item = Item(request.form)
        conn = connect()
        item = ItemDatabase(conn).push(new_item)
        conn.commit()
    return render_template("input.html")

@app.route('/view', methods=["GET", "POST"])
def view():
    conn = connect()
    items = ItemDatabase(conn).pull()
    conn.commit()
    return render_template("view.html", items=items)

@app.route('/login', methods=["GET", "POST"])
def login():
    if request.method == "POST" and request.form:
        conn = connect()
        user = UserDatabase(conn).load(request.form['username'])
        conn.commit()
        if user.password == request.form['password']:
            return render_template("cat.html")
    return render_template("login.html")

@app.route('/register', methods=["GET", "POST"])
def register():
    if request.form:
        conn = connect()
        user = UserDatabase(conn).store(request.form['username'],request.form['password'], request.form['email'])   
        conn.commit()
    return render_template("reg.html")

if __name__ == "__main__":
    app.run(debug=True)